# 0.2.0

- Abort iteration on error, preventing infinite loops in common iteration patterns
- Edition 2018

# 0.1.1

- Fix parsing hex device nodes

# 0.1.0

- Initial release
