use super::*;

use byteorder::ReadBytesExt;
use std::io::Seek;

use std::{fs, io};

/// Metadata about a specific virtual page.
#[derive(Debug)]
pub struct VirtualPage {
    /// See Documentation/vm/pagemap.txt in the kernel source
    data: u64,
}

impl VirtualPage {
    /// Page frame number, if present.
    /// On linux 4.2+, even if the page is present this will be `None` if the user does not have `CAP_SYS_ADMIN` (e.g. not root).
    ///
    /// To get the physical address of the start of the page, multiply by the page size.
    pub fn page_frame(&self) -> Option<u64> {
        // bits 0-54
        let page_frame = self.data & (u64::max_value() >> 9);
        if self.is_present() && page_frame > 0 {
            Some(page_frame)
        } else {
            None
        }
    }

    /// Swap type, if swapped.
    pub fn swap_type(&self) -> Option<u8> {
        // bits 0-4
        let swap_type = (self.data & 0x0F) as u8;
        match self.is_swapped() {
            true => Some(swap_type),
            false => None,
        }
    }

    /// Swap offset, if swapped.
    pub fn swap_offset(&self) -> Option<u64> {
        // bits 5-54
        let swap_offset = (self.data & (u64::max_value() >> 9)) >> 5;
        match self.is_swapped() {
            true => Some(swap_offset),
            false => None,
        }
    }

    /// Page table entry is soft-dirty
    pub fn is_soft_dirty(&self) -> bool {
        self.bit_set(55)
    }

    pub fn is_exclusively_mapped(&self) -> bool {
        self.bit_set(56)
    }

    /// Page is a file page or shared anon
    pub fn is_file_page_shared_anon(&self) -> bool {
        self.bit_set(61)
    }

    pub fn is_swapped(&self) -> bool {
        self.bit_set(62)
    }

    pub fn is_present(&self) -> bool {
        self.bit_set(63)
    }

    fn bit_set(&self, bit_index: u8) -> bool {
        (self.data & (1 << bit_index)) > 0
    }
}

/// Read page map for a virtual page.
///
/// The virtual page num can be calculated from a virtual address by dividing by the page size.
pub fn read_page_map(pid: ProcessId, virtual_page_num: usize) -> io::Result<VirtualPage> {
    let path = match pid {
        ProcessId::SelfPid => String::from("/proc/self/pagemap"),
        ProcessId::Num(n) => format!("/proc/{}/pagemap", n),
    };

    let mut f = fs::File::open(path)?;
    // Each entry is 8 bytes wide
    let offset = virtual_page_num as u64 * 8;
    f.seek(io::SeekFrom::Start(offset))?;

    let data = f.read_u64::<byteorder::NativeEndian>()?;

    Ok(VirtualPage { data })
}
