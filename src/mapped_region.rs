use std::io::BufRead;
use std::{fmt, fs, io, num, path};

use super::regex;

use super::ProcessId;

/// Permissions for a mapped region.
#[derive(Clone)]
pub struct Permissions {
    read: bool,
    write: bool,
    execute: bool,
    shared: bool,
}

impl Permissions {
    pub fn read(&self) -> bool {
        self.read
    }
    pub fn write(&self) -> bool {
        self.write
    }
    pub fn execute(&self) -> bool {
        self.execute
    }
    /// True iff `private()` is false.
    pub fn shared(&self) -> bool {
        self.shared
    }
    /// True iff `shared()` is false.
    pub fn private(&self) -> bool {
        !self.shared
    }
}

impl fmt::Debug for Permissions {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let read = if self.read { 'r' } else { '-' };
        let write = if self.write { 'w' } else { '-' };
        let execute = if self.execute { 'x' } else { '-' };
        let shared = if self.shared { 's' } else { 'p' };
        write!(f, "{}{}{}{}", read, write, execute, shared)
    }
}

/// Metadata for a mapped virtual memory region. See the `proc(5)` manpage.
#[derive(Clone, Debug)]
pub struct MemoryRegion {
    /// Start of the address in the process's address space.
    pub start_address: usize,
    pub end_address: usize,
    pub permissions: Permissions,
    /// Offset into the mapped file
    pub offset: usize,
    /// Device major number
    // 12 bits currently
    pub dev_major: u32,
    /// Device minor number
    // 20 bits
    pub dev_minor: u32,
    /// inode, if available
    pub inode: Option<u64>,
    /// Filename, or pseudo-path (e.g. `[stack]`), or `None` for anonymous mappings
    pub pathname: Option<String>,
}

#[derive(Debug)]
pub enum Error {
    ParseFailed(String),
    IoError(io::Error),
}

impl From<io::Error> for Error {
    fn from(e: io::Error) -> Self {
        Error::IoError(e)
    }
}

impl From<num::ParseIntError> for Error {
    fn from(err: num::ParseIntError) -> Self {
        Error::ParseFailed(format!("{:?}", err))
    }
}

/// Iterator across VM regions.
pub struct MemoryRegionIter<R: io::Read> {
    reader: io::BufReader<R>,
    buf: String,
    finished: bool,
}

impl<R: io::Read> MemoryRegionIter<R> {
    fn parse_buf(&mut self) -> Result<MemoryRegion, Error> {
        lazy_static! {
            static ref RE: regex::Regex = regex::Regex::new(
                "^([0-9a-f]+)-([0-9a-f]+) ([a-z-]{4}) ([0-9a-f]+) ([0-9a-f]+):([0-9a-f]+) (\\d+) +(.*)$")
                .expect("regex is valid");
        }

        let trimmed = self.buf.trim_end_matches('\n');
        let captures = RE
            .captures(trimmed)
            .ok_or_else(|| Error::ParseFailed(trimmed.to_string()))?;
        let start_address = usize::from_str_radix(&captures[1], 16)?;
        let end_address = usize::from_str_radix(&captures[2], 16)?;
        let p_bytes = &captures[3].as_bytes();
        let permissions = Permissions {
            read: p_bytes[0] == b'r',
            write: p_bytes[1] == b'w',
            execute: p_bytes[2] == b'x',
            shared: p_bytes[3] == b's',
        };
        let offset = usize::from_str_radix(&captures[4], 16)?;
        let dev_major = u32::from_str_radix(&captures[5], 16)?;
        let dev_minor = u32::from_str_radix(&captures[6], 16)?;
        let inode = u64::from_str_radix(&captures[7], 10)?;
        let inode = if inode == 0 { None } else { Some(inode) };
        let pathname = &captures[8];
        let pathname = if pathname.is_empty() {
            None
        } else {
            Some(String::from(pathname))
        };

        Ok(MemoryRegion {
            start_address,
            end_address,
            permissions,
            offset,
            dev_major,
            dev_minor,
            inode,
            pathname,
        })
    }
}

impl<R: io::Read> Iterator for MemoryRegionIter<R> {
    type Item = Result<MemoryRegion, Error>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.finished {
            return None;
        }

        self.buf.clear();

        match self.reader.read_line(&mut self.buf) {
            Ok(bytes_read) => {
                if bytes_read == 0 {
                    self.finished = true;
                    None
                } else {
                    Some(self.parse_buf())
                }
            }
            Err(e) => {
                // if reading hits an error, we likely will never succeed again, so we should
                // stop iterating
                self.finished = true;
                Some(Err(Error::IoError(e)))
            }
        }
    }
}

/// Load all mappings out of Linux procfs.
///
/// This may fail depending on permissions setup.
pub fn iter_mappings(pid: ProcessId) -> io::Result<MemoryRegionIter<fs::File>> {
    let path = path::PathBuf::from(match pid {
        ProcessId::SelfPid => String::from("/proc/self/maps"),
        ProcessId::Num(n) => format!("/proc/{}/maps", n),
    });

    let reader = fs::File::open(path)?;

    Ok(iter_mapping_reader(reader))
}

fn iter_mapping_reader<R: io::Read>(reader: R) -> MemoryRegionIter<R> {
    let buf_reader = io::BufReader::new(reader);

    MemoryRegionIter {
        reader: buf_reader,
        buf: String::new(),
        finished: false,
    }
}

#[cfg(test)]
mod tests {
    extern crate libc;

    use super::*;
    use std::path;

    #[test]
    fn sample_maps_file() {
        let path = path::Path::new("src/test-data/obexd-map.txt");
        let reader = fs::File::open(path).unwrap();

        let mappings = iter_mapping_reader(reader)
            .map(|r| r.unwrap())
            .collect::<Vec<MemoryRegion>>();

        assert_eq!(80, mappings.len());

        let first = &mappings[0];
        assert_eq!(94858956906496, first.start_address);
        assert_eq!(94858957352960, first.end_address);
        assert!(first.permissions.read());
        assert!(!first.permissions.write());
        assert!(first.permissions.execute());
        assert!(!first.permissions.shared());
        assert!(first.permissions.private());
        assert_eq!(0, first.offset);
        assert_eq!(8, first.dev_major);
        assert_eq!(3, first.dev_minor);
        assert_eq!(Some(58219319), first.inode);
        assert_eq!(
            "/usr/libexec/bluetooth/obexd",
            first.pathname.as_ref().unwrap()
        );

        let anon = &mappings[12];
        assert_eq!(0, anon.offset);
        assert_eq!(0, anon.dev_major);
        assert_eq!(0, anon.dev_minor);
        assert_eq!(None, anon.inode);
        assert_eq!(None, anon.pathname.as_ref());

        // has offset
        let libpthread = &mappings[50];
        assert!(!libpthread.permissions.read());
        assert!(!libpthread.permissions.write());
        assert!(!libpthread.permissions.execute());
        assert!(!libpthread.permissions.shared());
        assert!(libpthread.permissions.private());
        assert_eq!(106496, libpthread.offset);

        // different permissions, has inode
        let gconv = &mappings[71];
        assert!(gconv.permissions.read());
        assert!(!gconv.permissions.write());
        assert!(!gconv.permissions.execute());
        assert!(gconv.permissions.shared());
        assert!(!gconv.permissions.private());
        assert_eq!(3, gconv.dev_minor);
        assert_eq!(Some(55705632), gconv.inode);

        let last = &mappings[79];
        assert_eq!(18446744073699065856, last.start_address);
        assert_eq!(18446744073699069952, last.end_address);
        assert!(last.permissions.read());
        assert!(!last.permissions.write());
        assert!(last.permissions.execute());
        assert!(!last.permissions.shared());
        assert!(last.permissions.private());
        assert_eq!(0, last.offset);
        assert_eq!(0, last.dev_major);
        assert_eq!(0, last.dev_minor);
        assert_eq!(None, last.inode);
        assert_eq!("[vsyscall]", last.pathname.as_ref().unwrap());
    }

    #[test]
    fn maps_file_with_hex_device_nodes() {
        let path = path::Path::new("src/test-data/issue-1.txt");
        let reader = fs::File::open(path).unwrap();
        let mappings = iter_mapping_reader(reader)
            .map(|r| r.unwrap())
            .collect::<Vec<MemoryRegion>>();

        assert_eq!(518, mappings.len());

        let hex_dev_node_map = &mappings[0];

        assert_eq!(0xFD, hex_dev_node_map.dev_major);
        assert_eq!(0x00, hex_dev_node_map.dev_minor);
    }

    #[test]
    fn read_self() {
        // can find stack
        assert_eq!(
            1,
            iter_mappings(ProcessId::SelfPid)
                .unwrap()
                .filter_map(|r| r.unwrap().pathname.clone())
                .filter(|p| p == "[stack]")
                .count()
        )
    }

    #[test]
    fn read_own_pid() {
        let pid = unsafe { libc::getpid() as u32 };

        // can find stack
        assert_eq!(
            1,
            iter_mappings(ProcessId::Num(pid))
                .unwrap()
                .filter_map(|r| r.unwrap().pathname.clone())
                .filter(|p| p == "[stack]")
                .count()
        )
    }

    #[test]
    fn permissions_debug() {
        let p1 = Permissions {
            read: false,
            write: false,
            execute: false,
            shared: false,
        };

        assert_eq!("---p", format!("{:?}", p1));

        let p2 = Permissions {
            read: true,
            write: true,
            execute: true,
            shared: true,
        };

        assert_eq!("rwxs", format!("{:?}", p2));
    }

    #[test]
    fn iteration_aborts_on_error() {
        let path = path::Path::new("src/test-data/issue-1.txt");
        let file_reader = fs::File::open(path).unwrap();
        let error_reader = ErroringReader {
            reader: file_reader,
            bytes_until_error: 500,
        };

        // if we don't properly abort iteration on error, this will infinite loop
        let mappings = iter_mapping_reader(error_reader)
            .filter_map(|r| r.ok())
            .collect::<Vec<MemoryRegion>>();

        // iteration stopped
        assert_eq!(5, mappings.len());
    }

    struct ErroringReader<R: io::Read> {
        reader: R,
        bytes_until_error: usize,
    }

    impl<R: io::Read> io::Read for ErroringReader<R> {
        fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
            if self.bytes_until_error == 0 {
                return Err(io::Error::from(io::ErrorKind::Other));
            }

            let bytes = self.reader.read(buf)?;

            let remaining_bytes = self.bytes_until_error.saturating_sub(bytes);

            let res = if remaining_bytes == 0 {
                // only read the available bytes
                Ok(self.bytes_until_error)
            } else {
                Ok(bytes)
            };

            self.bytes_until_error = remaining_bytes;

            res
        }
    }
}
