[![](https://img.shields.io/crates/v/vm-info.svg)](https://crates.io/crates/vm-info) [![](https://docs.rs/vm-info/badge.svg)](https://docs.rs/vm-info/)

Inspect Linux virtual memory structure for a process.

Ever wanted to write your own fancier version of `pmap`, or find a physical address (useful in embedded devices)? This will let you do it.

See `examples/show-all.rs` for an example of how to use this to find mapped vm regions and load their physical address.
